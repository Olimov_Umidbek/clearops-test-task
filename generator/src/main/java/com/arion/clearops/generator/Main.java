package com.arion.clearops.generator;

import java.util.ArrayDeque;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        Queue<Integer> queue = new ArrayDeque();

        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);

        queue.forEach(item -> {
            if (item.equals(1) || item.equals(2)) {
                queue.poll();
            }
        });
        System.out.println(queue.size());
        queue.forEach(System.out::println);
    }
}
