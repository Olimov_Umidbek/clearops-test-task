package com.arion.clearops.generator.service;

public interface GeneratorKafkaService {

    void sendMessage(String message);
}
