package com.arion.clearops.generator.service.impl;

import com.arion.clearops.generator.service.StringGenerator;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.stream.Collectors;

@Service
public class StringGeneratorServiceImpl implements StringGenerator {

    @Override
    public String generate() {

        return new Random()
                .ints(10, 65, 90)
                .mapToObj(i -> Character.toString((char)i))
                .collect(Collectors.joining());
    }
}
