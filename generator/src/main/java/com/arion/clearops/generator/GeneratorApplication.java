package com.arion.clearops.generator;

import com.arion.clearops.generator.service.GeneratorKafkaService;
import com.arion.clearops.generator.service.StringGenerator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratorApplication implements CommandLineRunner {

    @Autowired
    private StringGenerator generator;

    @Autowired
    private GeneratorKafkaService kafkaService;

    public static void main(String[] args) {
        SpringApplication.run(GeneratorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
           while (true) {
                kafkaService.sendMessage(generator.generate());
                Thread.sleep(1000);
           }
    }
}
