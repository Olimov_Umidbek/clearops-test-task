package com.arion.clearops.generator.service.impl;

import com.arion.clearops.generator.service.GeneratorKafkaService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;

@Service
public class GeneratorKafkaServiceImpl implements GeneratorKafkaService {

    private static final Logger LOGGER = Logger.getLogger(GeneratorKafkaServiceImpl.class.getName());

    private final Queue<String> queue = new ArrayDeque<>();
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Value(value = "${clearops.kafka.receiver.topic}")
    private String topic;

    public GeneratorKafkaServiceImpl(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void sendMessage(String message) {
        queue.add(message);
        queue.forEach(this::publishMessage);
    }

    private void publishMessage(String message) {
        ListenableFuture<SendResult<String, String>> future =
                kafkaTemplate.send(topic, message);

        future.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                LOGGER.warning("Message=[" + message +
                        "] didn't send, errorMessage=[" + ex.getMessage() + "]");
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                // message removed from queue
                queue.poll();

                LOGGER.info("Sent message=[" + message +
                        "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
        });
    }
}
