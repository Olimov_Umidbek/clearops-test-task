package com.arion.clearops.generator.service;

/**
 *
 */
public interface StringGenerator {

    /**
     *
     * @return random word
     */
    String generate();
}
