# Getting Started

###First step
[+] You must download kafka and configure it
[+] You should run zookeeper and kafka after kafka configuration  

### Second step
Build receiver module
```shell
cd receiver
./gradlew clean build
cd build/libs
java -jar receiver-0.0.1-SNAPSHOT.jar
```

Build generator module
```shell
cd generator
./gradlew clean build
cd build/libs
java -jar generator-0.0.1-SNAPSHOT.jar
```