package com.arion.clearops.receiver.repository;

import com.arion.clearops.receiver.dao.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {
}
