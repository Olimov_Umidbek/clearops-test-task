package com.arion.clearops.receiver.service;

public interface KafkaService {

    void listenReceiverGroupWords(String message);
}
