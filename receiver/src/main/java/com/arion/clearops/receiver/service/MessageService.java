package com.arion.clearops.receiver.service;

import java.util.Date;

public interface MessageService {

    void saveMessage(String message, Date date);
}
