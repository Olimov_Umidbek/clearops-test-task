package com.arion.clearops.receiver.dao;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
@Table(name = "messages")
public class MessageEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(updatable = false, length = 10, nullable = false)
    private String message;

    @Column(updatable = false, nullable = false)
    private Date createdDate;
}
