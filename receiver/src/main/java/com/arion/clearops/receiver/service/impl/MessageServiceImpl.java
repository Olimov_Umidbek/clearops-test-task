package com.arion.clearops.receiver.service.impl;

import com.arion.clearops.receiver.dao.MessageEntity;
import com.arion.clearops.receiver.repository.MessageRepository;
import com.arion.clearops.receiver.service.MessageService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.logging.Logger;

@Service
public class MessageServiceImpl implements MessageService {

    private static final Logger LOGGER = Logger.getLogger(MessageServiceImpl.class.getName());

    private final MessageRepository repository;

    public MessageServiceImpl(MessageRepository repository) {
        this.repository = repository;
    }

    @Override
    public void saveMessage(String message, Date date) {
        MessageEntity messageEntity = new MessageEntity();
        messageEntity.setMessage(message);
        messageEntity.setCreatedDate(date);

        repository.save(messageEntity);
    }
}
