package com.arion.clearops.receiver.service.impl;

import com.arion.clearops.receiver.service.KafkaService;
import com.arion.clearops.receiver.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.logging.Logger;

@Service
@RequiredArgsConstructor
public class KafkaServiceImpl implements KafkaService {

    private static final Logger LOGGER = Logger.getLogger(KafkaServiceImpl.class.getName());
    private final MessageService messageService;

    @KafkaListener(topics = "${clearops.kafka.receiver.topic}",
            groupId = "${clearops.kafka.receiver.groupId}")
    public void listenReceiverGroupWords(String message) {
        LOGGER.info("Received message=[" + message + "]");

        if (StringUtils.isNoneBlank(message)) {
            messageService.saveMessage(message, new Date());
        }
    }
}
