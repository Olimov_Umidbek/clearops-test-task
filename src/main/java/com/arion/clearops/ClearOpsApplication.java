package com.arion.clearops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClearOpsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClearOpsApplication.class, args);
    }

}
